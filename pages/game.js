import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React, {useRef, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import {useNavigation} from '@react-navigation/native';

// const {height, width} = Dimensions.get('window');

const Game = () => {
  const lottieRef = useRef(null);
  const navigation = useNavigation();
  const [limit, setLimit] = useState(20);
  const [coins, setCoins] = useState(0);

  const handleRedemNowPress = () => {
    navigation.navigate('Followers');
  };

  const handleTouch = () => {
    // Decrease the limit and play the animation only if the limit is greater than 0
    if (limit > 0) {
      setLimit(prevLimit => prevLimit - 1);
      setCoins(prevCoins => prevCoins + 20);
      if (lottieRef.current) {
        lottieRef.current.play();
      }
    }
  };

  return (
    <LinearGradient
      style={styles.radialGradient}
      colors={['#FD5', '#FD5', '#FF543E', '#DD4282', '#C837AB']}
      start={{x: -0.0814, y: 1.1461}}
      end={{x: 0.7, y: 0.8127}}>
      <ScrollView>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: 30,
              color: 'white',
              textAlign: 'center',
              top: 20,
              fontFamily: 'Poppins-Regular',
            }}>
            Gulak
          </Text>
          <Image
            source={require('../assets/ad.png')}
            style={{justifyContent: 'center', alignSelf: 'center', top: 40}}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              top: 70,
            }}>
            <View
              style={{
                width: wp(35),
                height: hp(10),
                borderRadius: 10,
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  marginLeft: 10,
                  marginTop: 20,
                  flexDirection: 'row',
                  gap: 12,
                }}>
                <Image source={require('../assets/limit.png')} />
                <Text
                  style={{
                    color: '#E14478',
                    fontSize: hp(2.5),
                    textAlign: 'center',
                    marginTop: -5,
                    fontFamily: 'Poppins-Regular',
                  }}>
                  Limit {'\n'} {limit}/20
                </Text>
              </View>
            </View>
            <View
              style={{
                width: wp(35),
                height: hp(10),
                borderRadius: 10,
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  marginLeft: 10,
                  marginTop: 20,
                  flexDirection: 'row',
                  gap: 12,
                }}>
                <Image source={require('../assets/coin.png')} />
                <Text
                  style={{
                    color: '#E14478',
                    fontSize: hp(2.5),
                    textAlign: 'left',
                    marginTop: -5,
                    fontFamily: 'Poppins-Regular',
                  }}>
                  Coins {'\n'} {coins}
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              top: 100,
              width: wp(80),
              height: hp(30),
              borderRadius: 10,
              alignSelf: 'center',
              backgroundColor: 'white',
              justifyContent: 'center',
              marginBottom: 120,
            }}>
            <TouchableOpacity onPress={handleTouch}>
              <LottieView
                ref={lottieRef}
                source={require('../Animation - 1704179350964.json')}
                style={{width: wp(80), height: hp(50)}}
                autoPlay={false} // Set autoPlay to false
                loop={false}
              />
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={handleRedemNowPress}>
            <View
              style={{
                width: wp(80),
                height: hp(10),
                borderRadius: 10,
                alignSelf: 'center',
                backgroundColor: '#FFF854',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: hp(3.5),
                  textAlign: 'center',
                  color: 'rgba(53, 38, 0, 1)',
                  fontFamily: 'Poppins-Bold',
                }}>
                Redeem Coins
              </Text>
            </View>
          </TouchableOpacity>
          <View style={{gap: 20, top: 30, marginBottom: 60}}>
            <Image
              source={require('../assets/ad.png')}
              style={{justifyContent: 'center', alignSelf: 'center'}}
            />
            <Image
              source={require('../assets/ad.png')}
              style={{justifyContent: 'center', alignSelf: 'center'}}
            />
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default Game;

const styles = StyleSheet.create({
  radialGradient: {
    flex: 1,
  },
});
