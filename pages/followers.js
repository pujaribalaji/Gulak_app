import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
const Followers = () => {
  const [activeSection, setActiveSection] = useState('likes');

  const navigation = useNavigation();

  const handlecoinpress = coins => {
    navigation.navigate('Insta', {coins});
  };

  return (
    <LinearGradient
      style={styles.radialGradient}
      colors={['#FD5', '#FD5', '#FF543E', '#DD4282', '#C837AB']}
      start={{x: -0.0814, y: 1.1461}}
      end={{x: 0.7, y: 0.8127}}>
      <ScrollView>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: 30,
              color: 'white',
              textAlign: 'center',
              top: 20,
              fontFamily: 'Poppins-Regular',
            }}>
            Gulak
          </Text>
          <Image
            source={require('../assets/ad.png')}
            style={{justifyContent: 'center', alignSelf: 'center', top: 40}}
          />
          <View
            style={{
              flexDirection: 'row',
              top: 100,
              justifyContent: 'space-evenly',
            }}>
            <TouchableOpacity onPress={() => setActiveSection('likes')}>
              <View
                style={[
                  styles.tabButton,
                  activeSection === 'likes' && styles.activeTab,
                ]}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: hp(2.3),
                    color: activeSection === 'likes' ? 'black' : 'white',
                    fontFamily: 'Poppins-Bold',
                  }}>
                  Likes
                </Text>
              </View>
            </TouchableOpacity>
            <View style={styles.line} />
            <TouchableOpacity onPress={() => setActiveSection('followers')}>
              <View
                style={[
                  styles.tabButton,
                  activeSection === 'followers' && styles.activeTab,
                ]}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: hp(2.3),
                    color: activeSection === 'followers' ? 'black' : 'white',
                    fontFamily: 'Poppins-Bold',
                  }}>
                  Followers
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          {/* Content for Likes Section */}
          {activeSection === 'likes' && (
            <View>
              <View
                style={{
                  gap: 30,
                  flex: 1,
                  flexDirection: 'row',
                  alignSelf: 'center',
                  top: 130,
                }}>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(1000)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/like.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      1K Likes
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 2,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          1000
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(2000)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/like.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      2K Likes
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 3,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          2000
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  gap: 30,
                  flex: 1,
                  flexDirection: 'row',
                  alignSelf: 'center',
                  top: 160,
                }}>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(4750)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/like.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      5K Likes
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 2,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          4750
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(9500)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/like.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      10K Likes
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 3,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          9500
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}

          {/* Content for Followers Section */}
          {activeSection === 'followers' && (
            <View>
              <View
                style={{
                  gap: 30,
                  flex: 1,
                  flexDirection: 'row',
                  alignSelf: 'center',
                  top: 130,
                }}>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(1000)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/follower.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      1K Followers
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 2,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          1000
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(2000)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/follower.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      2K Followers
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 3,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          2000
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  gap: 30,
                  flex: 1,
                  flexDirection: 'row',
                  alignSelf: 'center',
                  top: 160,
                }}>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(4750)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/follower.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      5K Followers
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 2,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          4750
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    padding: 15,
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => handlecoinpress(9500)}
                    style={{
                      alignSelf: 'center',
                      alignItems: 'center',
                      top: 0,
                    }}>
                    <Image
                      source={require('../assets/follower.png')}
                      style={{width: wp(15), height: hp(8)}}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(64, 43, 50, 1)',
                      }}>
                      10K Followers
                    </Text>
                    <View
                      style={{
                        padding: 3,
                        paddingRight: 15,
                        top: 3,
                        borderRadius: 5,
                        backgroundColor: '#F04C5B',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          gap: 10,
                          paddingRight: 15,
                        }}>
                        <Image
                          style={{width: wp(8), height: hp(4)}}
                          source={require('../assets/coin.png')}
                        />
                        <Text
                          style={{
                            fontSize: hp(2.8),
                            color: 'white',
                            fontFamily: 'Poppins-Bold',
                          }}>
                          9500
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          <View
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              gap: 30,
              top: 200,
              marginBottom: 210,
            }}>
            <Image source={require('../assets/ad.png')} />
            <Image source={require('../assets/ad.png')} />
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default Followers;

const styles = StyleSheet.create({
  radialGradient: {
    flex: 1,
  },
  tabButton: {
    width: wp(40),
    height: wp(10),
    justifyContent: 'center',
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  activeTab: {
    backgroundColor: '#FFE046',
  },
  line: {
    height: 3,
    width: wp(86.5),
    backgroundColor: '#FFE046',
    position: 'absolute',
    bottom: 0,
    left: wp(7), // Adjust the left position based on the tab width
  },
});
