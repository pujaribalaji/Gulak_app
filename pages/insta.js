import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native'; // Import useNavigation from React Navigation

const Insta = ({route}) => {
  const [url, setUrl] = useState('');
  const {coins} = route.params;
  const [redeemed, setRedeemed] = useState(false);

  const navigation = useNavigation();

  const handleBack = () => {
    // Navigate back when the close image is pressed
    navigation.goBack();
  };

  const handleRedeemCoins = async () => {
    try {
      // Send the URL to the backend for validation and storage
      const response = await fetch('https://gulak-app.onrender.com/redeem', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // Add the following line
          mode: 'cors',
        },
        body: JSON.stringify({url}),
      });

      const data = await response.json();

      if (response.ok) {
        // Set the 'redeemed' state to true when the redemption is successful
        setRedeemed(true);

        // Show a success message to the user
        Alert.alert('Success', data.message);
      } else {
        // Show an error message to the user
        Alert.alert('Error', data.error);
      }
    } catch (error) {
      console.error(error);
      // Handle other errors if needed
    }
  };

  return (
    <LinearGradient
      style={styles.radialGradient}
      colors={['#FD5', '#FD5', '#FF543E', '#DD4282', '#C837AB']}
      start={{x: -0.0814, y: 1.1461}}
      end={{x: 0.7, y: 0.8127}}>
      <ScrollView>
        <View style={{flex: 1, marginBottom: 50}}>
          <Text
            style={{
              fontSize: 30,
              color: 'white',
              textAlign: 'center',
              top: 20,
              fontFamily: 'Poppins-Regular',
            }}>
            Gulak
          </Text>
          <Image
            source={require('../assets/ad.png')}
            style={{justifyContent: 'center', alignSelf: 'center', top: 50}}
          />
          <View
            style={{
              padding: 15,
              alignSelf: 'center',
              backgroundColor: 'white',
              top: 80,
              marginBottom: 30,
              borderRadius: 20,
            }}>
            <View style={{alignSelf: 'flex-end', top: 10}}>
              <TouchableOpacity onPress={handleBack}>
                <Image
                  source={require('../assets/close.png')}
                  style={{width: 35, height: 35}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignSelf: 'center',
                top: 5,
                marginBottom: 30,
              }}>
              <Text
                style={{
                  fontSize: hp(3),
                  color: 'black',
                  fontFamily: 'Poppins-Bold',
                }}>
                Get Followers & Likes
              </Text>
            </View>
            <View style={{top: 10}}>
              <TextInput
                style={{
                  height: 70,
                  borderColor: '#E24576',
                  borderWidth: 1,
                  borderRadius: 5,
                  paddingLeft: 30,
                  marginHorizontal: 5,
                  marginBottom: 10,
                  fontSize: hp(2),
                  color: 'black',
                }}
                placeholder="Paste Profile URL"
                onChangeText={text => setUrl(text)}
                value={url}
              />
            </View>
            <TouchableOpacity onPress={handleRedeemCoins}>
              <View
                style={{
                  flexDirection: 'row',
                  gap: 20,
                  padding: 5,
                  top: 30,
                  alignSelf: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#F04C5B',
                }}>
                <Image
                  source={require('../assets/coin.png')}
                  style={{alignSelf: 'center'}}
                />
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: hp(2.5),
                    color: 'white',
                    fontFamily: 'Poppins-Bold',
                  }}>
                  Redeem {coins} Coins
                </Text>
              </View>
            </TouchableOpacity>
            <Image
              source={require('../assets/ad.png')}
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                top: 60,
                marginBottom: 60,
              }}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              gap: 30,
              top: 100,
              marginBottom: 90,
            }}>
            <Image source={require('../assets/ad.png')} />
            <Image source={require('../assets/ad.png')} />
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default Insta;

const styles = StyleSheet.create({
  radialGradient: {
    flex: 1,
  },
});
