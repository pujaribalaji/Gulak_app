import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Landing from './pages/landing';
import Game from './pages/game';
import Quotes from './pages/quotes';
import Followers from './pages/followers';
import Insta from './pages/insta';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Landing"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Landing" component={Landing} />
        <Stack.Screen name="Game" component={Game} />
        <Stack.Screen name="Quotes" component={Quotes} />
        <Stack.Screen name="Followers" component={Followers} />
        <Stack.Screen name="Insta" component={Insta} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
